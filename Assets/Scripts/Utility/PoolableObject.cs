﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableObject : MonoBehaviour
{
	public virtual void Activate()
	{
		if (gameObject.activeSelf)
			return; 

		gameObject.SetActive(true); 
	}

	public virtual void Deactivate()
	{
		if (!gameObject.activeSelf)
			return; 

		gameObject.SetActive(false); 
	}
}
