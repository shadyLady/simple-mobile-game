﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationHandler : MonoBehaviour
{
	public Action AnimationEnd; 

	public void OnEndAnimation()
	{
		if (AnimationEnd != null)
			AnimationEnd(); 
	}
}
