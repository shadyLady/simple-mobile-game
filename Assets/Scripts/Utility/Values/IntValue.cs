﻿using UnityEngine;

[CreateAssetMenu(fileName = "New IntValue", menuName = "BaseValues/IntValue")]
public class IntValue : GenericValue<int>
{
	protected override bool Equals(int other)
	{
		return value == other; 
	}
}
