﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; 

[CustomEditor(typeof(FloatValue))] 
public class FloatValueEditor : Editor
{
	private FloatValue _target;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector(); 

		_target = target as FloatValue;

		EditorGUI.BeginChangeCheck(); 

		_target.defaultValue = EditorGUILayout.FloatField("Value", _target.defaultValue);

		if (EditorGUI.EndChangeCheck())
			_target.Value = _target.defaultValue; 
	}
}
