﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(IntValue))]
public class IntValueEditor : Editor
{
	private IntValue _target;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector(); 

		_target = target as IntValue;

		EditorGUI.BeginChangeCheck();

		_target.defaultValue = EditorGUILayout.IntField("Value", _target.defaultValue);

		if (EditorGUI.EndChangeCheck())
			_target.Value = _target.defaultValue; 
	}
}
