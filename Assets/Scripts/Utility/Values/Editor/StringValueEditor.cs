﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StringValue))]
public class StringValueEditor : Editor
{
	private StringValue _target;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		_target = target as StringValue;

		EditorGUI.BeginChangeCheck();

		_target.defaultValue = EditorGUILayout.TextField("Value", _target.defaultValue);

		if (EditorGUI.EndChangeCheck())
			_target.Value = _target.defaultValue; 
	}

}
