﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new FloatValue", menuName = "BaseValues/FloatValue")] 
public class FloatValue : GenericValue<float>
{
	protected override bool Equals(float other)
	{
		return value == other; 
	}
}
