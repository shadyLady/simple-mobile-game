﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Point2 
{
	public int x, y;

	public Point2(int x = 0, int y = 0)
	{
		this.x = x;
		this.y = y; 
	}
}
