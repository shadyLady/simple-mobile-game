﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceManager : MonoBehaviour
{
	[SerializeField] 
	private GameManager gameManager;

	[SerializeField] 
	private GameObject gameInterface;

	[SerializeField]
	private GameObject resultInterface; 

	private void Awake()
	{
		gameManager.GameEnd += OnGameEnd;

		gameInterface.SetActive(true); 
		resultInterface.SetActive(true); 
	}

	private void OnGameEnd()
	{
		gameInterface.SetActive(false); 
	}
}
