﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultMenu : MonoBehaviour
{
	[SerializeField] 
	private GameManager gameManager; 

	private void Awake()
	{
		gameManager.GameEnd += OnGameEnd;
		gameObject.SetActive(false);
	}

	private void OnGameEnd()
	{
		gameObject.SetActive(true); 
	}

	private void OnDestroy()
	{
		gameManager.GameEnd -= OnGameEnd; 
	}

	public void OnRetryButtonClicked()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(0); 
	}
}
