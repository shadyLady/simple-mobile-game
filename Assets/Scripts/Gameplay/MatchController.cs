﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Grid))]
public class MatchController : MonoBehaviour
{
	public delegate void MatchEvent(Match match);

	public MatchEvent MatchMade;

	private GameManager gameManager; 

	private Camera mainCamera; 

	private Grid grid;

	[SerializeField]
	[Tooltip("Minimum amount of quads required to make a match")] 
	private int minMatchAmount = 2; 

	private void Awake()
	{
		mainCamera = Camera.main;

		gameManager = GameManager.Instance; 

		grid = GetComponent<Grid>();
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
			GetQuad(); 
	}

	private void GetQuad()
	{
		Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit; 

		if (Physics.Raycast(ray, out hit))
		{
			Quad quad = hit.collider.GetComponent<Quad>();

			if (quad == null)
				return;

			FindMatches(quad); 
		}
	}

	private void FindMatches(Quad origin)
	{
		Match match = new Match();
		match.origin = origin;

		match.matchedList.Add(origin);
		match.visitedList.Add(origin);

		for(int matchIdx = 0; matchIdx < match.matchedList.Count; ++matchIdx)
		{
			List<Quad> neighbours = FindNeighbours(match.matchedList[matchIdx]);

			foreach (Quad neighbour in neighbours)
			{
				if (match.visitedList.Contains(neighbour))
					continue;

				match.visitedList.Add(neighbour);

				if (neighbour == null)
					continue;

				if (neighbour.Color == origin.Color)
				{
					match.matchedList.Add(neighbour);
				}
			}
		}

		match.points = match.matchedList.Count;

		if (match.matchedList.Count < minMatchAmount)
			return; 

		if (MatchMade != null)
			MatchMade(match); 
	}

	private List<Quad> FindNeighbours(Quad origin)
	{
		List<Quad> neighbours = new List<Quad>();

		int sx = origin.GridPosition.x - 1;
		int ex = origin.GridPosition.x + 1;

		int sy = origin.GridPosition.y - 1;
		int ey = origin.GridPosition.y + 1;

		if(sx >= 0)
			neighbours.Add(grid[sx, origin.GridPosition.y]);

		if(ex < grid.columnCount)
			neighbours.Add(grid[ex, origin.GridPosition.y]);

		if(sy >= 0)
			neighbours.Add(grid[origin.GridPosition.x, sy]);

		if(ey < grid.rowCount)
			neighbours.Add(grid[origin.GridPosition.x, ey]); 

		return neighbours;
	}
}
