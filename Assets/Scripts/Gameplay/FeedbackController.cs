﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MatchController))]
[RequireComponent(typeof(ObjectPool))]
public class FeedbackController : MonoBehaviour
{
	private MatchController matchController;

	private Grid grid;

	[SerializeField]
	private IntValue score;

	private ObjectPool feedbackPool; 

	private void Awake()
	{
		score.Value = 0;

		matchController = GetComponent<MatchController>();
		matchController.MatchMade += OnMatchMade;

		grid = GetComponent<Grid>();

		feedbackPool = GetComponent<ObjectPool>();
		feedbackPool.Create(10); 
	}

	private void OnMatchMade(Match match)
	{
		grid.AddSequence(ShowFeedback(match));
	}

	private IEnumerator ShowFeedback(Match match)
	{
		MatchFeedback feedback = feedbackPool.New() as MatchFeedback; 

		Vector3 feedbackPosition = match.origin.transform.position;
		feedbackPosition.z -= 8;

		feedback.Activate();
		feedback.gameObject.transform.position = feedbackPosition;

		foreach (Quad quad in match.matchedList)
		{
			quad.Deactivate();
		}

		feedback.feedbackValue.Value = match.points;
		score.Value += match.points;

		feedback.SetParticleColor(match.origin.Color);
		feedback.Show();

		grid.AddSequence(grid.Collapse(match.matchedList));

		yield return null;
	}
}
