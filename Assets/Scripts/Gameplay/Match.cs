﻿using System.Collections.Generic;
using UnityEngine;

public class Match
{
	public List<Quad> matchedList = new List<Quad>();

	public List<Quad> visitedList = new List<Quad>(); 

	public Color matchColor;

	public Quad origin;

	public int points; 
}
