﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
	public delegate void GameEvent();

	public GameEvent GameEnd; 

	[SerializeField]
	private Grid grid;

	private Timer intervalTimer;

	private bool gameEnd = false; 

	private void Start()
	{
		intervalTimer = GetComponentInChildren<Timer>();

		if (intervalTimer == null)
			Debug.LogWarning("No interval timer found");
		else
		{
			InitializeGame();
		}
	}

	private void InitializeGame()
	{
		grid.RowCountReached += OnRowCountReached; 
		grid.InitializeGrid();

		intervalTimer.StartTimer(OnIntervalTimerEnd); 
	}

	private void OnIntervalTimerEnd()
	{
		if (gameEnd)
		{
			intervalTimer.StopTimer();
			return; 
		}

		grid.AddSequence(grid.AddBottom());

		intervalTimer.StartTimer(OnIntervalTimerEnd); 
	}

	private void OnRowCountReached()
	{
		grid.RowCountReached -= OnRowCountReached;
		intervalTimer.StopTimer();

		gameEnd = true;

		if (GameEnd != null)
			GameEnd(); 
	}

	private void OnDestroy()
	{
		intervalTimer.StopTimer();

		grid.RowCountReached -= OnRowCountReached; 
	}
}
