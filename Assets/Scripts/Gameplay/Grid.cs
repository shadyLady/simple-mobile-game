﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GridGenerator))]
public class Grid : MonoBehaviour
{
	public delegate void GridEvent();

	public GridEvent RowCountReached;

	public readonly int columnCount = 9;

	public readonly int rowCount = 15;

	[SerializeField]
	[Tooltip("number of rows generated at level start")]
	private const int START_ROW_COUNT = 1;

	private GridGenerator generator;

	private Quad[,] quads;

	public Quad this[int x, int y]
	{
		get { return quads[x, y]; }
	}

	private Queue<IEnumerator> motionSequence = new Queue<IEnumerator>();

	public void InitializeGrid()
	{
		generator = GetComponent<GridGenerator>();

		quads = new Quad[columnCount, rowCount];

		for (int rowIdx = 0; rowIdx < START_ROW_COUNT; rowIdx++)
			motionSequence.Enqueue(AddBottom());

		StartCoroutine(Run()); 
	}

	//Motion sequencing
	private IEnumerator Run()
	{
		IEnumerator sequence;

		while (true)
		{
			if (motionSequence.Count > 0)
				sequence = motionSequence.Dequeue();
			else sequence = null;

			if (sequence != null)
			{
				StartCoroutine(sequence);
				yield return sequence;
			}

			yield return null;

		}
	}

	public void AddSequence(IEnumerator sequence)
	{
		motionSequence.Enqueue(sequence);
	}

	public IEnumerator MoveQuadUpRoutine(Quad quad)
	{
		yield return MoveQuadUp(quad);
	}

	public IEnumerator MoveQuadDownRoutine(Quad quad, Point2 position)
	{
		yield return MoveQuadDown(quad, position);
	}

	public IEnumerator AddBottom()
	{
		AddBottomRow();
		yield return null;
	}

	public IEnumerator Collapse(List<Quad> originList)
	{
		CollapseGrid(originList);
		yield return null;
	}

	//Actual motions
	private void AddBottomRow()
	{
		List<Quad> rowToAdd = generator.GenerateRow(columnCount);

		for (int columnIdx = 0; columnIdx < columnCount; ++columnIdx)
		{
			Quad quad = quads[columnIdx, 0];

			if (quad != null)
			{
				if (!MoveQuadUp(quad))
					return; 
			}

			Quad newQuad = rowToAdd[columnIdx];
			newQuad.MoveTo(new Point2(columnIdx, 0));
			newQuad.Activate();

			newQuad.Deactivated += OnQuadDeactivated; 

			quads[columnIdx, 0] = newQuad;
		}
	}

	private bool MoveQuadUp(Quad quad)
	{
		Point2 gridPosition = quad.GridPosition;

		if (quads[quad.GridPosition.x, quad.GridPosition.y] != quad)
			return false;

		int y = gridPosition.y + 1;

		//Check if we're going out of the grid yet
		if (y >= rowCount)
		{
			if (RowCountReached != null)
				RowCountReached(); 

			return false; 
		}

		//Is there a quad we should move before we can? 
		Quad topQuad = quads[gridPosition.x, y];

		if (topQuad != null && topQuad != quad)
		{
			if (!MoveQuadUp(topQuad))
				return false; 
		}

		//Move the quad up
		quads[gridPosition.x, y] = quad;
		quad.MoveTo(new Point2(gridPosition.x, y));

		return true; 
	}

	public bool MoveQuadDown(Quad quad, Point2 gridPosition)
	{
		if (quad != null && quads[quad.GridPosition.x, quad.GridPosition.y] != quad)
			return false;

		int y = gridPosition.y - 1;

		//Can't be moved down anymore
		if (y < 0)
			return false;

		//Check if there's another quad below, move that one before moving the current quad
		Quad lowerQuad = quads[gridPosition.x, y];

		if (lowerQuad != null && lowerQuad != quad)
		{
			//If the quad below us can't move down, we can't move down either
			if (!MoveQuadDown(lowerQuad, lowerQuad.GridPosition))
				return false;
		}

		//Move the quad down
		Quad currentQuad = quads[gridPosition.x, gridPosition.y]; 

		quads[gridPosition.x, y] = currentQuad;

		if(currentQuad != null)
			currentQuad.MoveTo(new Point2(gridPosition.x, y));

		return true;
	}

	private void OnQuadDeactivated(Quad quad)
	{
		quad.name = "Inactive"; 

		quads[quad.GridPosition.x, quad.GridPosition.y] = null; 

		quad.Deactivated -= OnQuadDeactivated; 
	}

	private IEnumerator MoveColumnRight(int columnIdx)
	{
		for (int idx = columnIdx; idx >= 0; idx--)
		{
			for (int rowIdx = 0; rowIdx < rowCount; rowIdx++)
			{
				Quad quad = quads[idx, rowIdx];

				if (quad == null)
					continue;

				quads[idx + 1, rowIdx] = quad;
				quad.MoveTo(new Point2(idx + 1, rowIdx)); 

				quads[idx, rowIdx] = null; 
			}
		}

		yield return null; 
	}

	public void CollapseGrid(List<Quad> originList)
	{
		List<Point2> columns = new List<Point2>(); 

		foreach(Quad quad in originList)
		{
			Point2 column = new Point2(quad.GridPosition.x, 0);

			if (!columns.Contains(column))
				columns.Add(column); 
		}

		foreach (Point2 column in columns)
		{
			for (int rowIdx = 0; rowIdx < rowCount; rowIdx++)
			{
				Quad quad = quads[column.x, rowIdx];

				if (quad != null)
					continue;

				//Search for first filled quad above the current empty one
				for (int rwIdx = rowIdx + 1; rwIdx < rowCount; rwIdx++)
				{
					Quad topQuad = quads[column.x, rwIdx];

					if (topQuad == null)
						continue;

					topQuad.MoveTo(new Point2(column.x, rowIdx));
					quads[column.x, rowIdx] = topQuad;
					quads[column.x, rwIdx] = null;

					break; 
				}
			}
		}

		CollapseToRight();
	}

	private void CollapseToRight()
	{
		for (int columnIdx = columnCount - 1; columnIdx > 0; columnIdx--)
		{
			Quad quad = quads[columnIdx, 0];

			if (quad != null)
				continue;

			for (int searchIdx = columnIdx - 1; searchIdx >= 0; searchIdx--)
			{
				if (quads[searchIdx, 0] == null)
					continue;

				for (int rowIdx = 0; rowIdx < rowCount; rowIdx++)
				{
					Quad neighbour = quads[searchIdx, rowIdx];

					if (neighbour == null)
						break; 

					quads[searchIdx, rowIdx] = null;
					quads[columnIdx, rowIdx] = neighbour;
					quads[columnIdx, rowIdx].MoveTo(new Point2(columnIdx, rowIdx));
				}

				break;
			}
		}
	}
}
