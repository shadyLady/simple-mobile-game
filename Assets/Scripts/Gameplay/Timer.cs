﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 

public class Timer : MonoBehaviour
{
	[SerializeField]
	private float durationInSeconds = 1.0f; 

	public float Duration
	{
		get { return durationInSeconds; }
		set { durationInSeconds = value; }
	}

	[SerializeField] 
	private IntValue currentTime; 

	private Action callBack;

	private IEnumerator timeRoutine; 

	public void StartTimer(Action callBack)
	{
		currentTime.Value = 0;

		this.callBack = callBack;

		if (timeRoutine != null)
		{
			StopCoroutine(timeRoutine);
			timeRoutine = null;
		}

		timeRoutine = RunTimer();
		StartCoroutine(timeRoutine); 
	}

	public void StopTimer()
	{
		if (timeRoutine != null)
		{
			StopCoroutine(timeRoutine);
			timeRoutine = null; 
		}

		callBack = null; 
	}

	private IEnumerator RunTimer()
	{
		while (currentTime.Value <= durationInSeconds)
		{
			yield return new WaitForSeconds(1.0f);
			currentTime.Value++; 
		}

		if (callBack != null)
			callBack(); 
	}
}
