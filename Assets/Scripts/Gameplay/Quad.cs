﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quad : PoolableObject
{
	public delegate void QuadEvent(Quad quad);

	public QuadEvent ColorUpdated;

	public QuadEvent PositionUpdated;

	public QuadEvent Deactivated; 

	private Point2 gridPosition;

	private Color color;

	//Temp variable 
	private MeshRenderer meshRenderer; 

	public Color Color
	{
		get { return color; } 
		set
		{
			color = value;

			if (meshRenderer == null)
				meshRenderer = GetComponent<MeshRenderer>();

			meshRenderer.material.color = color; 

			if (ColorUpdated != null)
				ColorUpdated(this); 
		} 
	}

	public Point2 GridPosition
	{
		get { return gridPosition; }
		set
		{
			gridPosition = value;

			if (PositionUpdated != null)
				PositionUpdated(this); 
		} 
	}

	public void MoveTo(Point2 position)
	{
		transform.position = new Vector3(position.x, position.y);
		gridPosition = position;

		gameObject.name = "Quad (" + gridPosition.x + ", " + gridPosition.y + ")"; 
	}

	public override void Deactivate()
	{
		if (Deactivated != null)
			Deactivated(this);

		base.Deactivate(); 
	}
}
