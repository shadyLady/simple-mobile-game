﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
	[SerializeField]
	private Quad quadPrefab;

	[SerializeField]
	private List<Color> allColorOptions;

	private int colorsAllowedOnStart = 2;

	private List<Color> colorOptions;

	private int generatedRowCount = 0;

	private int addColorOnCount = 5; 

	private void Awake()
	{
		colorOptions = new List<Color>();

		if (allColorOptions.Count < colorsAllowedOnStart)
		{
			Debug.Log("Not enough colors to start with");
			return; 
		}

		for (int colorIdx = 0; colorIdx < colorsAllowedOnStart; colorIdx++)
			AddNewColor();
	}

	public List<Quad> GenerateRow(int columnCount)
	{
		generatedRowCount++;

		if (generatedRowCount >= addColorOnCount)
		{
			generatedRowCount = 0;
			AddNewColor();
		}

		List<Quad> generatedQuads = new List<Quad>();

		for (int columnIdx = 0; columnIdx < columnCount; ++columnIdx)
		{
			Quad quad = Instantiate(quadPrefab);

			quad.gameObject.SetActive(false);
			quad.transform.SetParent(transform, false);

			Color color = colorOptions[Random.Range(0, colorOptions.Count)];
			quad.Color = color; 

			generatedQuads.Add(quad);
		}

		return generatedQuads;
	}

	private void AddNewColor()
	{
		if (allColorOptions.Count <= 0)
			return;

		int idx = Random.Range(0, allColorOptions.Count);
		colorOptions.Add(allColorOptions[idx]);
		allColorOptions.RemoveAt(idx);
	}
}
