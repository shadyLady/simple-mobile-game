﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
	[SerializeField]
	private PoolableObject prefab;

	[SerializeField] 
	private Transform feedbackParent;

	private List<PoolableObject> pooledObjects = new List<PoolableObject>(); 

	public void Create(int initialSize, PoolableObject prefab = null)
	{
		if (prefab != null)
			this.prefab = prefab;

		if (this.prefab == null)
		{
			Debug.LogError("No prefab assigned");
			return; 
		}

		CreateMultiple(initialSize); 
	}

	private PoolableObject CreateNew()
	{
		PoolableObject obj = Instantiate(prefab);
		obj.Deactivate(); 

		if (feedbackParent != null)
			obj.transform.SetParent(feedbackParent); 

		obj.name = "PooledObject";

		pooledObjects.Add(obj);
		return obj;
	}

	public PoolableObject New()
	{
		foreach (PoolableObject obj in pooledObjects)
		{
			if (!obj.isActiveAndEnabled)
				return obj; 
		}

		return CreateNew(); 
	}

	private void CreateMultiple(int amount)
	{
		int currentAmount = 0;

		while (currentAmount < amount)
		{
			currentAmount++;

			CreateNew();
		}
	}
}
