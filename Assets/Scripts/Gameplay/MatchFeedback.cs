﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchFeedback : PoolableObject
{
	private ParticleSystem feedbackParticles;

	private AnimationHandler animationHandler; 

	private float feedbackDuration = 0.7f;

	public IntValue feedbackValue;

	private void Awake()
	{
		feedbackParticles = GetComponentInChildren<ParticleSystem>(true);

		animationHandler = GetComponentInChildren<AnimationHandler>(true);

		if (feedbackParticles == null)
			Debug.LogWarning("No particle system found amongst children");
	}

	public void SetParticleColor(Color color)
	{
		color.a = 255;

		var main = feedbackParticles.main;
		main.startColor = color; 
	}

	public void Show()
	{
		if (animationHandler != null)
		{
			animationHandler.gameObject.SetActive(true);
			animationHandler.AnimationEnd += OnAnimationEnd; 
		}

		feedbackParticles.gameObject.SetActive(true); 
	}

	private void OnAnimationEnd()
	{
		feedbackParticles.gameObject.SetActive(false);

		if(animationHandler != null)
			animationHandler.AnimationEnd -= OnAnimationEnd; 

		Deactivate();
	}
}
